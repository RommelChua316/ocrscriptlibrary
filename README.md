PLEASE NOTE THAT THE PROJECT ONLY HAS THE MINIMUM REQUIRED POM DEPENDENCIES DEFINED FOR ITS CLIENT CODE TO GET IT TO COMPILE.
TO ACTUALLY RUN THE CODE IN YOUR MACHINE, YOU NEED A COPY OF ALL THE LIBRARIES IN \Ephesoft\Application\WEB-INF\lib

## **INSTALLATION INSTRUCTION**

To release this library, build the JAR file by running a Maven build (mvn clean install).
This generates a JAR file in the "target" folder. Copy the JAR file into \Ephesoft\Application\WEB-INF\lib\.

The build will also generate the properties file for this project. Copy the properties file into \Ephesoft\Application\WEB-INF\classes.
Update the value for the appropriate Clovelly URL.