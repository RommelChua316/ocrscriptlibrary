package com.mogoplus.properties;

import java.io.InputStream;
import java.util.Properties;

public class PropertyReader {
    public static String getPropertyValue(String key) throws Exception {
        Properties prop = new Properties();
        InputStream propertyFileStream = PropertyReader.class.getClassLoader().getResourceAsStream("scriptlibrary.properties");
        prop.load(propertyFileStream);

        return prop.getProperty(key);
    }
}
