package com.mogoplus;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mogoplus.client.ClovellyClient;
import com.mogoplus.model.AuditLogEntry;
import com.sun.jersey.api.client.ClientResponse;
import org.jdom.Document;
import org.jdom.Element;

public class OCRUtils {
    public static final String NIL = "NIL";
    public static final String TRANSACTION_DATE = "TransactionDate";
    public static final String NEWEST_TO_OLDEST = "NewestToOldest";
    public static final String OLDEST_TO_NEWEST = "OldestToNewest";
    //ephesoft script step
    public static final String PAGEPROCESS_MODULE = "PAGEPROCESS";
    public static final String DOCASSEMBLY_MODULE = "DOCASSEMBLY";
    public static final String AUTOVALIDATE_MODULE = "AUTOVALIDATE";
    public static final String EXPORT_MODULE = "EXPORT";
    //clovelly status strings
    private static final String DOCRUNNING_STATUS = "DOCRUNNING";
    private static final String DOCREVIEW_STATUS = "DOCREVIEW";
    private static final String DOCVALIDATION_STATUS = "DOCVALIDATION";
    private static final String DOCFINISHED_STATUS = "DOCFINISHED";
    //ephesoft dashboard status strings
    private static final String DASHBOARD_RUNNING = "Running";
    private static final String DASHBOARD_READY_FOR_REVIEW = "Ready For Review";
    private static final String DASHBOARD_READY_FOR_VALIDATION = "Ready For Validation";
    private static final String DASHBOARD_FINISHED = "Finished";

    private static final String MODULE_NAME = "Ephesoft";

    public static String extractBankCode(Document document) {
        String batchClassName = document.getRootElement().getChildText("BatchClassName");
        if (batchClassName != null && !batchClassName.equals("")) {
            return batchClassName.toUpperCase().replace("MOGOPLUS4", "").trim();
        }
        return "";
    }

    //METHOD FOR NOTIFICATION/AUDIT LOGS
    public static ClientResponse generateAuditLogEntry(String module, Document document) throws Exception {
        List<Element> documents =  document.getRootElement().getChild("Documents").getChildren("Document");
        ClovellyClient client = new ClovellyClient();
        String bankCode = extractBankCode(document);

        AuditLogEntry logEntry = new AuditLogEntry();
        logEntry.setModuleName(MODULE_NAME);
        logEntry.setAccessId(document.getRootElement().getChildText("BatchName"));
        logEntry.setNotifySupport(false);
        logEntry.setBatchNumber("");

        if (PAGEPROCESS_MODULE.equals(module)) {
            logEntry.setStatus(DOCRUNNING_STATUS);
            logEntry.setNotifyProvider(false);
            logEntry.setDescription(DASHBOARD_RUNNING + (bankCode.equals("")?"":" - " + bankCode));
            return client.createOcrStatus(logEntry);
        } else if (DOCASSEMBLY_MODULE.equals(module)) {
            boolean sendAuditLogEntry = false;
            for (Element doc: documents) {
                if (doc.getChildText("Type").equals("Unknown")) {
                    sendAuditLogEntry = true;
                    break;
                }
            }
            if (sendAuditLogEntry) {
                logEntry.setStatus(DOCREVIEW_STATUS);
                logEntry.setNotifyProvider(true);
                logEntry.setDescription(DASHBOARD_READY_FOR_REVIEW + (bankCode.equals("")?"":" - " + bankCode));
                return client.createOcrStatus(logEntry);
            }
        } else if (AUTOVALIDATE_MODULE.equals(module)) {
            boolean sendAuditLogEntry = false;
            for (Element doc: documents) {
                if (doc.getChildText("Valid").equals("false")) {
                    sendAuditLogEntry = true;
                    break;
                }
            }
            if (sendAuditLogEntry) {
                logEntry.setStatus(DOCVALIDATION_STATUS);
                logEntry.setNotifyProvider(true);
                logEntry.setDescription(DASHBOARD_READY_FOR_VALIDATION + (bankCode.equals("")?"":" - " + bankCode));
                return client.createOcrStatus(logEntry);
            }

        } else if (EXPORT_MODULE.equals(module)) {
            logEntry.setStatus(DOCFINISHED_STATUS);
            logEntry.setNotifyProvider(false);
            logEntry.setDescription(DASHBOARD_FINISHED + (bankCode.equals("")?"":" - " + bankCode));
            return client.createOcrStatus(logEntry);
        }
        return null;
    }

    //METHODS FOR OCR CORRECTION
    /**
     * fixes the following ocr errors on numeric fields:
     * finds multiple "." and changes all BUT THE LAST ONE to ","
     * finds "O" and replaces it with "0"
     * finds "l" or "I" or "|" and replaces it with "1"
     */
    public static String fixNumericOcrErrors(String value) {
        return fixNumericOcrErrors(value, false); //default is to never fix missing .
    }

    public static String fixNumericOcrErrors(String value, boolean fixMissingPeriod) {
        if (value != null) {
            //replace middle dots with proper period
            String temp = value.replaceAll("\u00B7", ".").trim();

            //optional step for NAB... add missing .
            if (temp.length() > 2 && fixMissingPeriod) {
                if (temp.charAt(temp.length() - 3) != '.') {
                    if (temp.charAt(temp.length() - 3) == ',') {
                        temp = temp.substring(0, temp.length() - 3) + "." + temp.substring(temp.length() - 2);
                    } else {
                        temp = temp.substring(0, temp.length() - 2) + "." + temp.substring(temp.length() - 2);
                    }
                }
            }

            //only one . allowed... replace other . with ,
            int ind = temp.lastIndexOf(".");
            if (ind != -1) {
                String left = temp.substring(0, ind);
                String right = temp.substring(ind);
                temp = left.replaceAll("\\.", ",") + right;
            }

            //fix 0's and 1's
            temp = temp.replaceAll("[OQ]", "0").replaceAll("[lI|]","1");
            return temp;
        } else {
            return null;
        }
    }

    /**
     * fixes the following ocr errors:
     * replaces \/ with V
     * replaces VV or \/\/ with W
     */
    public static String fixCommonOcrErrors(String value) {
        if (value != null && !value.isEmpty()) {
            return value.replaceAll("\\\\/", "V").replaceAll("VV", "W");
        }
        return value;
    }

    //NAME AND ADDRESS METHODS
    /**
     * given a customer name and address block string, this will find the start of the address block
     */
    public static int findAddressIndex(String nameAndAddress) {
        String[] addressStrings = {"\\d"," APARTMENT "," APT "," BLOCK "," FLAT "," LEVEL "," LOT "," LVL "," OFFICE "," ROOM "," RM "," SHOP "," SITE "," STALL "," SUITE "," UNIT "," VILLA "," BASEMENT FLOOR "," GROUND FLOOR "," LOWER GROUND FLOOR "," MEZZANINE "," UPPER GROUND FLOOR "," TOWNHOUSE "," TOWN HOUSE "," PO BOX "," GPO BOX "," NO."};
        int firstOccurence = -1;
        for(String addressString: addressStrings) {
            Pattern pattern = Pattern.compile("(?i)" + addressString);
            Matcher matcher = pattern.matcher(nameAndAddress);
            if (matcher.find()) {
                if (firstOccurence == -1) {
                    firstOccurence = matcher.start();
                } else if (firstOccurence > matcher.start()) {
                    firstOccurence = matcher.start();
                }
            }
        }
        return firstOccurence;
    }

    //TABLE DATE CALCULATION METHODS
    public static long dateDifference(Date startDate, Date endDate) {
        if (startDate != null && endDate != null) {
            long diffInMillis = Math.abs(endDate.getTime() - startDate.getTime());
            return TimeUnit.DAYS.convert(diffInMillis, TimeUnit.MILLISECONDS);
        }
        return -1;
    }

    public static String getSortOrder(String previousFormattedDateString, String formattedDateString) {
        String[] currentDateParts = getDateParts(formattedDateString);
        String[] previousDateParts = getDateParts(previousFormattedDateString);
        if (currentDateParts != null && previousDateParts != null) {
            try {
                int currentDay = Integer.parseInt(currentDateParts[0]);
                int currentMonth = Integer.parseInt(currentDateParts[1]);
                int previousDay = Integer.parseInt(previousDateParts[0]);
                int previousMonth = Integer.parseInt(previousDateParts[1]);

                if (previousMonth == currentMonth) {
                    if (currentDay < previousDay) {
                        return NEWEST_TO_OLDEST;
                    }
                    if (previousDay < currentDay) {
                        return OLDEST_TO_NEWEST;
                    }
                } else {
                    //assume previous date is from year before and current date is in current year
                    int case1 = currentMonth + (13 - previousMonth);
                    //assume current date is from year before and previous date is in current year
                    int case2 = previousMonth + (13 - currentMonth);
                    //assume previous and current date is in same year
                    int case3 = Math.abs(previousMonth - currentMonth) + 1;

                    int smallest = case1;
                    if (case2 < smallest) smallest = case2;
                    if (case3 < smallest) smallest = case3;

                    if (smallest == case1) {
                        return OLDEST_TO_NEWEST;
                    } else if (smallest == case2) {
                        return NEWEST_TO_OLDEST;
                    } else if (smallest == case3) {
                        if (currentMonth < previousMonth) {
                            return NEWEST_TO_OLDEST;
                        } else {
                            return OLDEST_TO_NEWEST;
                        }
                    }
                }
            } catch (Exception e) {
            }
        }
        return "";
    }

    public static String[] getDateParts(String date) {
        if (!date.isEmpty()) {
            String[] dateParts = date.split("/");
            if (dateParts.length == 3) return dateParts;
        }
        return null;
    }

    public static void repopulateTransactionDate(Element transactions, String sortOrder, Date startDate, Date endDate, String startYear, String endYear) {
        List<Element> transactionRowsList = transactions.getChild("Rows").getChildren("Row");
        if (transactionRowsList != null && transactionRowsList.size() > 0) {
            String currYear;
            Date previousTransactionDate = null;

            if (sortOrder.equals(NEWEST_TO_OLDEST)) {
                currYear = endYear;
            } else {
                currYear = startYear;
            }

            for (Element row: transactionRowsList) {
                Element value = findDateForRow(row);

                String dateString = getElementValue(value);
                Date tempDate = getDateFromString(dateString);

                if (tempDate != null) { //means date is valid format
                    dateString = dateString.substring(0,6) + currYear;
                    tempDate = getDateFromString(dateString);
                    if (inDateRange(dateString, startDate, endDate)) {
                        if (sortOrder.equals(NEWEST_TO_OLDEST)) {
                            if (previousTransactionDate != null && previousTransactionDate.before(tempDate)) {
                                currYear = startYear;
                            }
                        } else {
                            if (previousTransactionDate != null && previousTransactionDate.after(tempDate)) {
                                currYear = endYear;
                            }
                        }
                    } else {
                        if (sortOrder.equals(NEWEST_TO_OLDEST)) {
                            currYear = startYear;
                        } else {
                            currYear = endYear;
                        }
                    }
                    dateString = dateString.substring(0,6) + currYear;
                    tempDate = getDateFromString(dateString);
                    setElementValue(value, dateString);
                    if (tempDate != null) {
                        previousTransactionDate = tempDate;
                    }
                } //end if valid date
            } //end row loop
        }
    }

    public static boolean inDateRange(String date, Date startDate, Date endDate) {
        Date currentDate = getDateFromString(date);
        if (currentDate != null) {
            if ((currentDate.equals(startDate) || currentDate.after(startDate)) && (currentDate.equals(endDate) || currentDate.before(endDate))) {
                return true;
            }
        }
        return false;
    }

    //TABLE REVERSING METHODS
    /**
     * reverses table
     */
    public static void reverseTable(Element transactions) {
        List<Element> transactionRowsList = transactions.getChild("Rows").getChildren("Row");
        List<Element> reversedTransactionRowsList = new ArrayList<>();
        List<Element> rowsToDetach = new ArrayList<>();
        if (transactionRowsList != null && tableNeedsReversing(transactionRowsList)) {
            for (int ind = transactionRowsList.size() - 1; ind >= 0; ind --) {
                reversedTransactionRowsList.add(transactionRowsList.get(ind));
                rowsToDetach.add(transactionRowsList.get(ind));
            }
            rowsToDetach.forEach(t -> t.detach());
            for (Element row: reversedTransactionRowsList) {
                transactions.getChild("Rows").addContent(row);
            }
        }
    }

    public static boolean tableNeedsReversing(List<Element> transactionRowsList) {
        if (transactionRowsList != null && (transactionRowsList.size() > 1)) {
            Date previousDate = null;
            for (Element row: transactionRowsList) {
                Element value = findDateForRow(row);
                Date currentDate = getDateFromString(getElementValue(value));
                if (previousDate != null && currentDate != null) {
                    if (previousDate.before(currentDate)) return true;
                    if (previousDate.after(currentDate)) return false;
                }
                previousDate = currentDate;
            }
        }
        return false;
    }

    public static Element findDateForRow(Element row) {
        if (row != null) {
            List<Element> colList = row.getChild("Columns").getChildren("Column");
            if (colList != null) {
                for (Element col: colList) {
                    Element name = col.getChild("Name");
                    Element value = col.getChild("Value");

                    value = createElementIfMissing(value, col);

                    if (elementIs(name, TRANSACTION_DATE)) {
                        return value;
                    }
                }
            }
        }
        return null;
    }

    public static Date getDateFromString(String date) {
        try {
            return new SimpleDateFormat("dd/MM/yyyy").parse(date);
        } catch (Exception e) {
            return null;
        }
    }

    //DATE FORMATTING METHODS
    /**
     * formats date according to d MMM yyyy pattern
     */
    public static String formatDate(String date) {
        return formatDate(date, null);
    }

    /**
     * formats date according to provided pattern
     */
    public static String formatDate(String date, String inputPattern) {
        String out = "";
        try {
            if (inputPattern == null || inputPattern.isEmpty()) {
                inputPattern = "d MMM yyyy";
            }
            SimpleDateFormat sourceDateFormat = new SimpleDateFormat(inputPattern, Locale.UK);
            SimpleDateFormat outputDateFormat = new SimpleDateFormat("dd/MM/yyyy");
            out = outputDateFormat.format(sourceDateFormat.parse(date));
        } catch (Exception e) {
        }
        return out;
    }

    /**
     * adds "20" in front of year of date value which is assumed to be day (single or double digit), month (numeric, short or long forms), year (two digits)
     */
    public static String fixDateYear(String date, String delimiter, String outputDelimiter) {
        String out = "";
        try {
            String dateParts[] = date.split(delimiter);
            if (dateParts.length != 3) return out;
            String day = dateParts[0];
            String month = dateParts[1];
            String year = dateParts[2];
            if (year.length() == 2) {
                year = "20" + year;
            }
            return day + outputDelimiter + month + outputDelimiter + year;
        } catch (Exception e) {
        }
        return out;
    }

    public static String fixDateYear(String date, String delimiter) {
        return fixDateYear(date, delimiter, " ");
    }

    //METHODS FOR NUMERICS
    public static String formatBalance(String value, boolean invertNegative) {
        value = value.toUpperCase();
        boolean isNegative = false;
        if (value==null) {
            return "";
        } else {
            if (invertNegative) {
                if (value.contains("+") || value.contains("DR") || (!value.contains("-") && !value.contains("CR"))) {
                    isNegative = true;
                }
            } else {
                if (value.contains("-") || value.contains("DR")) {
                    isNegative = true;
                }
            }
            value = value.replaceAll("(CR|DR|\\-|\\+|\\s|\\$|\u00A3)", "").trim();
            if (isNegative && !value.equals("") && !value.equals("0.00")) {
                return "-" + value;
            } else {
                return value;
            }
        }
    }
    //METHODS FOR READING DLFS AND ROW COLUMNS IN ADVANCE
    public static Map<String, Element> getColumnsForRow(Element row) {
        Map<String, Element> columnsForRow = new HashMap<>();
        if (row != null) {
            List<Element> colList = row.getChild("Columns").getChildren("Column");
            if (colList != null) {
                for (Element col: colList) {
                    Element name = col.getChild("Name");
                    Element value = col.getChild("Value");
                    value = createElementIfMissing(value, col);
                    columnsForRow.put(getElementValue(name), value);
                }
            }
        }
        return columnsForRow;
    }

    public static Map<String, Element> getDlfsForDocument(Element doc) {
        Map<String, Element> dlfsForDocument = new HashMap<>();

        List<Element> documentLevelFields = doc.getChild("DocumentLevelFields").getChildren("DocumentLevelField");
        if (documentLevelFields != null) {
            for (Element dlf: documentLevelFields) {
                Element name = dlf.getChild("Name");
                Element value = dlf.getChild("Value");
                value = createElementIfMissing(value, dlf);
                dlfsForDocument.put(getElementValue(name), value);
            }
        }
        return dlfsForDocument;
    }

    //METHODS FOR ACCESSING AND TESTING ELEMENTS
    public static boolean valueIsEmptyString(Element value) {
        return getElementValue(value).equals("");
    }

    public static Element createElementIfMissing(Element value, Element parent) {
        if (value == null) { //create value element only if it is missing
            value = new Element("Value").setText("");
            parent.addContent(1, value);
        }
        return value;
    }

    public static boolean isNil(String value) {
        if (value == null || value.toUpperCase().equals(NIL)) return true;
        return false;
    }

    public static boolean elementIs(Element element, String id) {
        if (element == null || id == null || id.equals("")) return false;
        return id.equals(getElementValue(element));
    }

    public static String getElementValue(Element element) {
        String value = "";
        if (element != null && element.getText() != null) {
            value = element.getText().trim();
        }
        return value;
    }

    public static void setElementValue(Element element, String value) {
        if (element!=null) element.setText(value);
    }
}
