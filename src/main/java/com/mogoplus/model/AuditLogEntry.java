package com.mogoplus.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AuditLogEntry {
    private String moduleName;
    private String accessId;
    private String status;
    private boolean notifySupport;
    private boolean notifyProvider;
    private String description;
    private String batchNumber;

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getAccessId() {
        return accessId;
    }

    public void setAccessId(String accessId) {
        this.accessId = accessId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isNotifySupport() {
        return notifySupport;
    }

    public void setNotifySupport(boolean notifySupport) {
        this.notifySupport = notifySupport;
    }

    public boolean isNotifyProvider() {
        return notifyProvider;
    }

    public void setNotifyProvider(boolean notifyProvider) {
        this.notifyProvider = notifyProvider;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }
}
