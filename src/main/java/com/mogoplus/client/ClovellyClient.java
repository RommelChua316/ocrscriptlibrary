package com.mogoplus.client;

import com.mogoplus.model.AuditLogEntry;
import com.mogoplus.properties.PropertyReader;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import javax.ws.rs.core.MediaType;


public class ClovellyClient {
    //update the url to localhost:$port_number before building...
    //private static final String CLOVELLY_URL = "http://192.168.103.45:8085/clovelly"; //FOR DEV!!!!
    //private static final String CLOVELLY_URL = "http://localhost:12085/clovelly";
    //private static final String CLOVELLY_URL = "http://localhost:14085/clovelly";
    private static final String CLOVELLY_URL = "http://localhost:8085/clovelly";
    //private static final String CLOVELLY_URL = "http://10.51.8.141:8085/clovelly";
    private static Client client = Client.create();

    public ClientResponse createOcrStatus(AuditLogEntry logEntry) throws Exception {
        WebResource r = client.resource(PropertyReader.getPropertyValue("url.clovelly") + "/portal/ocrStatus");
        ClientResponse response = r.accept(MediaType.APPLICATION_JSON_TYPE).entity(logEntry, MediaType.APPLICATION_JSON).post(ClientResponse.class);
        return response;
    }
}